# order  

## endpoints  
| endpoint | purpose |
| -------- | ------- |
|          |         |

## core tables  
| table | purpose | references |
| ----- | ------- | ---------- |
|       |         |            |

    order.order
    order.quote
    order.request
    order.requirement
    order.return
    order.shoppingcart
    order.shoppinglist
    order.opportunity
