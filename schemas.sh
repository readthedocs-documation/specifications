#!/bin/sh
while IFS="," read -r module table
do    
    echo $module $table
    mv schemas/$table.yaml $module/schemas
done <  module-table.csv
