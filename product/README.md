# product  

## endpoints  
| endpoint | purpose |
| -------- | ------- |
|          |         |

## core tables  
| table | purpose | references |
| ----- | ------- | ---------- |
|       |         |            |

    product.catalog
    product.category
    product.config
    product.cost
    product.facility
    product.feature
    product.inventory
    product.price
    product.product
    product.promo
    product.store
    product.subscription
    product.supplier
