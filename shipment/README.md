# shipment  

## endpoints  
| endpoint | purpose |
| -------- | ------- |
|          |         |

## core tables  
| table | purpose | references |
| ----- | ------- | ---------- |
|       |         |            |

  shipment.issuance
  shipment.picklist
  shipment.receipt
  shipment.shipment
