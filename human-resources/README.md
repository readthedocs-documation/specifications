# human-resources  

## endpoints  
| endpoint | purpose |
| -------- | ------- |
|          |         |

## core tables  
| table | purpose | references |
| ----- | ------- | ---------- |
|       |         |            |


  humanres.ability
  humanres.employment
  humanres.position
