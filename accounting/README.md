# accounting  

## endpoints  
| endpoint | purpose |
| -------- | ------- |
|          |         |

## core tables  
| table | purpose | references |
| ----- | ------- | ---------- |
|       |         |            |



accounting.budget
accounting.finaccount
accounting.fixedasset
accounting.invoice
accounting.ledger
accounting.payment
accounting.rate
accounting.tax
