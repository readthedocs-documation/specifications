#!/bin/sh
while IFS="," read -r module
do    
    echo $module
    mkdir -p $module/parameters/headers
    mkdir -p $module/parameters/paths
    mkdir -p $module/parameters/queries
    mkdir -p $module/parameters/cookies

    touch $module/parameters/headers/.gitkeep
    touch $module/parameters/paths/.gitkeep
    touch $module/parameters/queries/.gitkeep
    touch $module/parameters/cookies/.gitkeep
    
    mkdir -p $module/resources
    mkdir -p $module/responses
    mkdir -p $module/schemas

    touch $module/resources/.gitkeep
    touch $module/responses/.gitkeep
    touch $module/schemas/.gitkeep

    cat > $module/openapi.yaml <<EOF
openapi: "3.0.0"
info:
  version: 1.0.0
  title: $module
  description: $module specification
  license:
    name: MIT
  contact:
    name: $module
    url: http://www.google.com/support
    email: $module@google.com
servers:
  - url: http://$module.swagger.io/v1
tags:
  - name: $module
EOF

    cat > $module/README.md <<EOF
# $module  

## endpoints  
| endpoint | purpose |
| -------- | ------- |
|          |         |

## core tables  
| table | purpose | references |
| ----- | ------- | ---------- |
|       |         |            |

EOF
done <  modules.csv
