# marketing  

## endpoints  
| endpoint | purpose |
| -------- | ------- |
|          |         |

## core tables  
| table | purpose | references |
| ----- | ------- | ---------- |
|       |         |            |

    marketing.campaign
    marketing.contact
    marketing.segment
    marketing.tracking
    marketing.opportunity
