# party  

## endpoints  
| endpoint | purpose |
| -------- | ------- |
|          |         |

## core tables  
| table | purpose | references |
| ----- | ------- | ---------- |
|       |         |            |

  party.agreement
  party.communication
  party.contact
  party.need
  party.party
